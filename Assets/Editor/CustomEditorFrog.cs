﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Frog))]
public class CustomEditorFrog : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        Frog frog = (Frog) target;
        GUILayout.BeginHorizontal();
    
        if (GUILayout.Button("Speed + 1"))
        {
            frog.speed++;
        }

        if (GUILayout.Button("Speed - 1"))
        {
            frog.speed--;
        }
        
        if (GUILayout.Button("Apply to all"))
        {
            Frog[] spidersScript = FindObjectsOfType<Frog>();
            Debug.Log("a");
            foreach (Frog script in spidersScript)
            {
                script.speed = frog.speed;
                Debug.Log("b");
            }
        }
        
        GUILayout.EndHorizontal();
    }
}
