﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Spider))]
public class CustomEditorSpider : Editor
{
 
  public override void OnInspectorGUI()
  {
    base.OnInspectorGUI();
    Spider spider = (Spider) target;
    GUILayout.BeginHorizontal();
    
    if (GUILayout.Button("Speed + 1"))
    {
     spider.speed++;
     
    }

    if (GUILayout.Button("Speed - 1"))
    {
     spider.speed--;
     
    }

    if (GUILayout.Button("Apply to all"))
    {
      Spider[] spidersScript = FindObjectsOfType<Spider>();
      Debug.Log("a");
      foreach (Spider script in spidersScript)
      {
        script.speed = spider.speed;
        Debug.Log("b");
      }
    }
    
    GUILayout.EndHorizontal();
  }
}
