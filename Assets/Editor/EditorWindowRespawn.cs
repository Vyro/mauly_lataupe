﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class EditorWindowRespawn : EditorWindow
{
   [MenuItem("Window/Respawn")]
   public static void ShowWindow()
   {
      GetWindow<EditorWindowRespawn>("Respawn");
   }

   void OnGUI()
   {
      if (Selection.gameObjects.Length > 0)
      {
         GUILayout.Label("Selected object : " + Selection.gameObjects[0].name, EditorStyles.boldLabel);
      }

      if (GUILayout.Button("Respawn"))
      {
         foreach (GameObject obj in Selection.gameObjects)
         {
            Respawn respawn = obj.GetComponent<Respawn>();
            if (respawn != null)
            {
               respawn.Respwan();
            }
         }
         
      }
   }
   
}
