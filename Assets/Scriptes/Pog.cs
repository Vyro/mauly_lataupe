﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pog : MonoBehaviour
{
    public float moveSpeed = 5.0f;
    public Rigidbody2D rb;
    private Vector2 _movement;
    public Animator animator;


    public AudioClip[] slimeStep;
    private float _timerStep;

    public GameObject lewisText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        _movement.x = Input.GetAxisRaw("Horizontal");
        _movement.y = Input.GetAxisRaw("Vertical");
        // Adapt the diagonal movement
        _movement.Normalize();

        if (lewisText.activeSelf == false)
        {
            animator.SetFloat("Horizontal", _movement.x);
            animator.SetFloat("Vertical", _movement.y);
            animator.SetFloat("Speed", _movement.sqrMagnitude);
            SoundMove();
        }
    }
    
    void FixedUpdate()
    {
        if (lewisText.activeSelf == false)
        {
            rb.MovePosition(rb.position + _movement * (moveSpeed * Time.fixedDeltaTime));
        }
        else
        {
            rb.MovePosition(rb.position + Vector2.zero);
        }
       
    }

    void SoundMove()
    {
        if (_movement.x > 0 || _movement.x < 0 || _movement.y > 0 || _movement.y < 0) // vérifie si le joueur bouge
        {
            if (lewisText.activeSelf == false)
            {
                if (_timerStep <= 0) // son de bruit de pas 
                {
                    int rdm = Random.Range(0, 3);
                    AudioSource.PlayClipAtPoint(slimeStep[rdm], transform.position, 1);
                    _timerStep = 0.35f;
                }

                if (_timerStep >= 0)
                {
                    _timerStep -= Time.deltaTime;
                }
            }
        }
    }
}
