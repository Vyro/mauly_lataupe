﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Autel : MonoBehaviour
{
    public int goldTotal;

    public int goldGiven;

    private bool _giveMuch;

    private bool _enouhgGold;

    public GameObject root;

    public AudioClip giveSound;

    public Animator animatorDemon;

    [SerializeField] private bool canDoAnimationDance = false;

    [SerializeField] private float timerAnimDanceDemon = 2f;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_enouhgGold)
        {
            Debug.Log("fini");
            root.SetActive(false);
        }

        // Animation dance demon
        if (canDoAnimationDance)
        {
            animatorDemon.SetBool("isDance", true);
            // timer
            timerAnimDanceDemon -= Time.deltaTime;
            
            // Stop animation
            if (timerAnimDanceDemon <= 0f)
            {
                animatorDemon.SetBool("isDance", false);
                canDoAnimationDance = false;
                timerAnimDanceDemon = 2f;
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        
        if (other.gameObject.CompareTag("Player"))
        {
            GoldPlayer player = other.gameObject.GetComponent<GoldPlayer>(); 
            
            if (goldTotal >= 0)
            {
                if (player.goldHold > 0)
                {
                    AudioSource.PlayClipAtPoint(giveSound, transform.position);
                    canDoAnimationDance = true;
                }

                player.goldHold += player.goldBonus; // ajoute le bonus a l'argent du joueur
                player.goldBonus = 0; // reset le bonus
                goldGiven +=player.goldHold; // ajoute l'argent du joueur dans l'hotel
                if (goldTotal <= goldGiven)
                {
                    _enouhgGold = true;
                }
                
            }
            else
            {
                _giveMuch = true;
            }
            
            if (_giveMuch == false)
            {
                if (goldTotal > 0) // s'il reste de l'argent a donné
                {
                    player.goldHold = 0; // réduit l'argent du joueur du nobre donné
                }
                else
                {
                    player.goldHold = 0; // réduit l'argent à 0
                }
            }
        }
    }
}
