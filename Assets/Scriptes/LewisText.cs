﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LewisText : MonoBehaviour
{
    public String[] line;
    public int actualLine;
    public TMP_Text textLewis;

    public int maxText;

    public Autel gold;
    // Start is called before the first frame update
    void Start()
    {
        
        textLewis.text = line[0];
    }

    // Update is called once per frame
    void Update()
    {
                  
           

            if (actualLine < line.Length)
            {
                textLewis.text = line[actualLine];
            }
            else
            {
                textLewis.text = "You brought " + gold.goldGiven + " to me out of " + gold.goldTotal;
            }
    }
}
