﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Frog : MonoBehaviour
{
    public int speed;
    public bool orientation;

    [SerializeField] private float timePause = 8f;
    public Animator animator;
    [SerializeField] private bool isPause = false;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (!isPause)
        {
            if (orientation)
            {
                transform.Translate(new Vector2(0, 1) * speed * Time.deltaTime);
                animator.SetFloat("Vertical", speed);
                animator.SetFloat("Horizontal", 0);
            }
            else
            {
                transform.Translate(new Vector2(1, 0) * speed * Time.deltaTime);
                animator.SetFloat("Vertical", 0);
                animator.SetFloat("Horizontal", speed);
            }
        }
        
    }

    public void Wait()
    {
        isPause = true;
        Debug.Log("isPause = " + isPause);
        while (timePause > 0f)
        {
            timePause -= Time.deltaTime;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        FrogCheck frogCheck = other.GetComponent<FrogCheck>();
        
        if (other.gameObject.CompareTag("BlockFrog"))
        {
            if (frogCheck != null)
            {
                if (frogCheck.GetDirection().Equals(FrogCheck.Direction.Droite))
                {
                    speed *= 1;
                }
                else
                {
                    speed *= -1;
                }
                orientation = !orientation;
            }
            // speed *= -1;
            
            Debug.Log("Speed = " + speed);
        }

        if (other.gameObject.CompareTag("BlockFrogEnd"))
        {
            if (frogCheck != null)
            {
                if (frogCheck.GetDirection().Equals(FrogCheck.Direction.Droite))
                {
                    speed *= 1;
                }
                else
                {
                    speed *= -1;
                }
            }
        }


        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<LifePog>().Damage(1);
        }
    }
}