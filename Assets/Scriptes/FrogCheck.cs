﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrogCheck : MonoBehaviour
{
    [SerializeField] private Direction direction;
    public enum Direction
    {
        Droite,
        Gauche
    }

    public Direction GetDirection()
    {
        return direction;
    }
}
