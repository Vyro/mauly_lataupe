﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demon : MonoBehaviour
{
    public GameObject dialogBox;
    private bool _once;
    private bool _stopDetection;
    public LayerMask player;
    public bool first;
    public LewisText scriptLine;
    // Start is called before the first frame update
    void Start()
    {
        if (first)
        {
            dialogBox.SetActive(true);
        }
       
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Accept") && dialogBox.activeSelf)
        {
            scriptLine.actualLine++;
            if (scriptLine.actualLine >= scriptLine.line.Length)
            {
                dialogBox.SetActive(false);
            }
        }
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            dialogBox.SetActive(true);
        }
        
    }
}
