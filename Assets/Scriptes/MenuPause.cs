﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPause : MonoBehaviour
{
    public GameObject menu;
    
    // Start is called before the first frame update
    void Start()
    {
        menu.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Cancel")) // ouvre le menu
        {
            menu.SetActive(true);
        }

        if (menu.active) // menu ouvert
        {
            Time.timeScale = 0; // empêche tout mouvement
            if (GameObject.FindGameObjectWithTag("Music") != null) // change le volume de la musique
            {
                GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = 0.15f;
            }
           
        }

        if (menu.active == false) // menu fermé
        {
            Time.timeScale = 1; // remet les mouvement 
            if (GameObject.FindGameObjectWithTag("Music") != null) // met le volume normal
            {
                GameObject.FindGameObjectWithTag("Music").GetComponent<AudioSource>().volume = 0.3f;
            }
     
        }
    }

    public void Resume() // ferme le menu
    {
        menu.SetActive(false);
    }

    public void Restart() // recharge le niveau en cours
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Quit() // quitte le jeu
    {
        Application.Quit();
    }
}
