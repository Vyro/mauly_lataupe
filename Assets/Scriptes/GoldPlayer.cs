﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldPlayer : MonoBehaviour
{
    public int goldBonus;
    public Pog pog;
    public int goldHold;
    public float moveInit;
    public TMP_Text goldUi;

    public AudioClip goldSound;
    // Start is called before the first frame update
    void Start()
    {
        moveInit = pog.moveSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        goldUi.text = goldHold.ToString() +  " (+ " + goldBonus.ToString() + ")";
        goldBonus = goldHold / 2;
        
        if (goldHold >= 10)
        {
            goldHold = 10;
        }

        if (goldHold <= 0)
        {
            pog.moveSpeed = moveInit;
          //  goldHold = 0;
          //  goldBonus = 0;
        }
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Gold"))
        {
            AudioSource.PlayClipAtPoint(goldSound, transform.position);
            goldHold++;
            pog.moveSpeed -= 0.20f;
            Destroy(other.gameObject);
        }

        if (other.gameObject.CompareTag("Reset"))
        {
            goldHold = 0;
        }
    }
}
