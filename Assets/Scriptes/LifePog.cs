﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LifePog : MonoBehaviour
{
    [SerializeField] private int maxLife = 3;
    [SerializeField] private TMP_Text textLife;
    [SerializeField] private float dodgeDuration = 1;
    private int _life;
    public SpriteRenderer sr;
    private bool isDodge;
    public GameObject respaawn;

    // Start is called before the first frame update
    void Start()
    {
        _life = maxLife;
    }

    // Update is called once per frame
    void Update()
    {
        textLife.text = _life + "/" + maxLife;
    }

    public void Damage(int damage)
    {
        if (!isDodge)
        {
            isDodge = true;
            _life -= damage;
            if (_life <= 0)
            {
                _life = 0;
                // Game Over
                respaawn.GetComponent<Respawn>().Respwan();
             //   FindObjectOfType<GameManager>().EndGame();
            }
           
           //UpdateLife();
            sr.DOColor(new Color(1, 1, 1, 0f), dodgeDuration / 6).SetLoops(6, LoopType.Yoyo);
            Invoke(nameof(EndDodge), dodgeDuration);
        }
    }
    
    public void EndDodge()
    {
        isDodge = false;
    }
}
