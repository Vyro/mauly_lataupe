﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;


public class Spider : MonoBehaviour
{
    public int speed;
    public bool orientation;
    public Animator animator;
    
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (orientation)
        { 
            transform.Translate(new Vector2(0, 1) * speed * Time.deltaTime);
            animator.SetFloat("Vertical", speed);
        }
        else
        {
            transform.Translate(new Vector2(1, 0) * speed * Time.deltaTime);
            animator.SetFloat("Horizontal", speed);
        }

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("BlockSpider"))
        {
            speed *= -1;
        }
        
       
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            other.gameObject.GetComponent<LifePog>().Damage(1);
            Debug.Log("b");
        }
    }
}
